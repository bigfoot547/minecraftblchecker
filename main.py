import sys
import requests
import re
import hashlib


def main(argv):
    if len(argv) < 2:
        sys.stderr.write("Usage: {} <server1> [server2] ...".format(argv[0]))
        return 1

    host = argv[1]
    print("Querying Minecraft server blacklist...")
    blacklist_response = getBlacklist()
    if blacklist_response[0] is None:
        sys.stderr.write("An error {} occurred while querying the blacklist.".format(blacklist_response[1]))
        return 1

    blacklist = blacklist_response[0]
    print("Found {} entries on the blacklist.".format(len(blacklist)))

    search = []

    for server in argv[1:]:
        steps = getSteps(server)
        for step in steps:
            search.append((server, step, hashlib.sha1(step.encode("ISO-8859-1")).hexdigest()))

    print("")
    print("Searching blacklist...")
    print("")

    found = {}
    one_found = False
    for search_tup in search:
        if search_tup[0] not in found:
            found[search_tup[0]] = 0

    for banned in blacklist:
        for search_tup in search:
            if search_tup[2] == banned:
                one_found = True
                found[search_tup[0]] += 1
                print("Matching blacklist entry found! Server: {}, Mask: {}, Hash: {}".format(search_tup[0], search_tup[1], search_tup[2]))

    if one_found:
        print("")

    print("Search complete! Summary of servers:")
    num_blacklisted = 0
    for key in found:
        val = found[key]
        if val > 0:
            num_blacklisted += 1
        print("{}: {} ({} blacklist entr{})".format(key, "Not blacklisted." if val == 0 else "Blacklisted!", val, "y" if val == 1 else "ies"))

    print("")
    print("Found {} blacklisted server{}.".format(num_blacklisted, "" if num_blacklisted == 1 else "s"))
    return 0

def getBlacklist():
    r = requests.get("https://sessionserver.mojang.com/blockedservers")
    if r.status_code == 200:
        return r.text.strip("\n").split("\n"), r.status_code
    else:
        return None, r.status_code


def getSteps(host):
    ip = re.match("^([0-9]{1,3})\.([0-9]{1,3})\.([0-9]{1,3})\.([0-9]{1,3})$", host)

    steps = []
    if ip is None:
        steps.append(host)
        parts = host.split(".")
        parts.reverse()
        partb = ""
        for part in parts:
            partb = "." + part + partb
            steps.append("*" + partb)
    else:
        steps.append(ip.group(0))
        steps.append(ip.group(1) + "." + ip.group(2) + "." + ip.group(3) + ".*")
        steps.append(ip.group(1) + "." + ip.group(2) + ".*.*")
        steps.append(ip.group(1) + ".*.*.*")
    return steps


if __name__ == "__main__":
    exit(main(sys.argv))
